import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavBarComponent } from './components/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/common/modules/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AllComponent } from './modules/users/components/all/all.component';
import { HomeComponent } from './components/home/home.component';
import { TweetComponent } from './components/tweet/tweet.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AllComponent,
    HomeComponent,
    TweetComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
