import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/common/models/user';
import { SessionService } from 'src/app/common/services/session.service';
import { PasswordsMatchValidator } from '../registration/registration.component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent {
  private password: FormControl = new FormControl('', [Validators.required]);
  private confirmPassword: FormControl = new FormControl('', [
    Validators.required,
  ]);

  public changePasswordForm = new FormGroup(
    {
      password: this.password,
      confirmPassword: this.confirmPassword,
    },
    { validators: PasswordsMatchValidator }
  );
  public hide: boolean = true;
  public viewChangePassword: boolean = false;
  public user!: User;

  constructor(private session: SessionService) {
    this.session.userDetails$.subscribe(
      (userDetails) => (this.user = userDetails)
    );
  }

  changePassword(): void {}

  toggleChangePassword() {
    this.viewChangePassword = !this.viewChangePassword;
    console.log(this.changePasswordForm.errors)
  }
}
