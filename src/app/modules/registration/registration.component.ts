import { Component } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  NgForm,
  FormGroupDirective,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
} from '@angular/forms';
import { User } from 'src/app/common/models/user';
import { TweetService } from 'src/app/common/services/tweet.service';
import { ErrorStateMatcher } from '@angular/material/core';
import { SessionService } from 'src/app/common/services/session.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent {
  constructor(
    private tweetService: TweetService,
    private session: SessionService
  ) {}

  hide: boolean = true;
  username: FormControl = new FormControl('', Validators.required);
  firstName: FormControl = new FormControl('', Validators.required);
  lastName: FormControl = new FormControl('', Validators.required);
  email: FormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  contactNumber: FormControl = new FormControl('', [Validators.required]);
  password: FormControl = new FormControl('', [Validators.required]);
  confirmPassword: FormControl = new FormControl('', [Validators.required]);

  public registrationForm = new FormGroup(
    {
      username: this.username,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      contactNumber: this.contactNumber,
      password: this.password,
      confirmPassword: this.confirmPassword,
    },
    { validators: PasswordsMatchValidator }
  );

  matcher = new MyErrorStateMatcher();

  onSubmit() {
    const user: User = {
      username: this.registrationForm.get('username')?.value,
      firstName: this.registrationForm.get('firstName')?.value,
      lastName: this.registrationForm.get('lastName')?.value,
      email: this.registrationForm.get('email')?.value,
      contactNumber: this.registrationForm.get('contactNumber')?.value,
    };

    const password: string = this.registrationForm.get('password')?.value;

    this.tweetService.registerNewUser(user, password).subscribe((response) => {
      this.session.logIn(response);
    });
  }
}

export const PasswordsMatchValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');

  return password && confirmPassword && password.value !== confirmPassword.value ? { passwordsNotMatching: true } : null;
};
