import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { TweetService } from 'src/app/common/services/tweet.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/common/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss'],
})
export class AllComponent implements OnInit {
  public displayedColumns: string[] = [
    'username',
    'firstName',
    'lastName',
    'email',
    'contactNumber',
  ];
  public searchPattern: string = localStorage.searchPattern || '';
  public pageSize: string = '10';
  public pageIndex: string = '0';
  public length!: number;
  public dataSource!: MatTableDataSource<User>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private tweetService: TweetService, private router: Router) {}

  ngOnInit(): void {
    this.getData();
  }

  private getData(): void {
    this.tweetService
    .searchUsers(this.searchPattern, this.pageSize, this.pageIndex)
    .subscribe((response) => {
      this.dataSource = new MatTableDataSource<User>(response.content);
      this.dataSource.paginator = this.paginator;
        this.length = response.numberOfElements;
      });
  }

  public viewUser(user: User){
    localStorage.setItem("savedUser", JSON.stringify(user));
    this.router.navigate(["/users/user"]);
  }
}
