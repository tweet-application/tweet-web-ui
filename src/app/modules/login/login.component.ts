import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/common/models/user';
import { SessionService } from 'src/app/common/services/session.service';
import { TweetService } from 'src/app/common/services/tweet.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  hide: boolean = true;
  username: FormControl = new FormControl('', Validators.required);
  password: FormControl = new FormControl('', Validators.required);

  public loginForm = new FormGroup({
    username: this.username,
    password: this.password,
  });

  constructor(
    private tweetService: TweetService,
    private session: SessionService
  ) {}

  onSubmit() {
    this.tweetService
      .logIn(
        this.loginForm.get('username')?.value,
        this.loginForm.get('password')?.value
      )
      .subscribe((user) => {
        this.session.logIn(user);
      });
  }
}
