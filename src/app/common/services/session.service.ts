import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private loggedInSource = new BehaviorSubject<boolean>(false);
  private userDetailsSource = new BehaviorSubject<User>(new User());

  public loggedIn$ = this.loggedInSource.asObservable();
  public userDetails$ = this.userDetailsSource.asObservable();

  constructor(private router: Router) {}

  logIn(user: User) {
    if (user) {
      this.loggedInSource.next(true);
      this.userDetailsSource.next(user);
      alert('Successfully logged in');
      this.router.navigate(['']);
    } else {
      alert('Invalid credentials');
    }
  }

  logOut() {
    this.loggedInSource.next(false);
    this.userDetailsSource.next(new User());
    this.router.navigate(['']);
  }
}
