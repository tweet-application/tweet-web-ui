import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/common/models/user';
import { TweetRequestBody } from 'src/app/common/models/tweetRequestBody';
import { SearchUserResponse } from 'src/app/common/models/searchUserResponse';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Tweet } from '../models/tweet';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class TweetService {
  constructor(private http: HttpClient) {}

  private base_url: string = environment.TWEET_SERVICE_URL;

  registerNewUser(user: User, password: string) {
    httpOptions.headers = httpOptions.headers.set('password', password);
    return this.http
      .post<User>(`${this.base_url}/register`, user, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  logIn(username: string, password: string) {
    httpOptions.headers = httpOptions.headers
      .set('username', username)
      .set('password', password);
    return this.http
      .get<User>(`${this.base_url}/login`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  resetPassword(username: string, password: string) {
    httpOptions.headers = httpOptions.headers.set('password', password);
    return this.http
      .get(`${this.base_url}/${username}/forgot`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  getAllUsers() {
    return this.http
      .get(`${this.base_url}/users/all`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  searchUsers(username: string, pageSize: string, pageNumber: string) {
    httpOptions.headers = httpOptions.headers
      .set('pageSize', pageSize)
      .set('pageNumber', pageNumber);
    return this.http
      .get<SearchUserResponse>(
        `${this.base_url}/user/search?username=${username}`,
        httpOptions
      )
      .pipe(retry(3), catchError(this.handleError));
  }

  getAllTweets() {
    return this.http
      .get<Tweet[]>(`${this.base_url}/all`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  getAllUserTweets(username: string) {
    return this.http
      .get<Tweet[]>(`${this.base_url}/${username}`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  postTweet(username: string, tweet: TweetRequestBody) {
    return this.http
      .post<Tweet>(`${this.base_url}/${username}/add`, tweet, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  updateTweet(username: string, id: string, tweet: TweetRequestBody) {
    return this.http
      .put<Tweet>(
        `${this.base_url}/${username}/update/${id}`,
        tweet,
        httpOptions
      )
      .pipe(retry(3), catchError(this.handleError));
  }

  deleteTweet(username: string, id: string) {
    return this.http
      .delete(`${this.base_url}/${username}/delete/${id}`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  likeTweet(username: string, id: string) {
    return this.http
      .put(`${this.base_url}/${username}/like/${id}`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  replyToTweet(username: string, id: string, tweet: TweetRequestBody) {
    return this.http
      .post<Tweet>(`${this.base_url}/${username}/reply/${id}`, tweet, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  getTweetReplies(id: string) {
    return this.http
      .get<Tweet[]>(`${this.base_url}/${id}/replies`, httpOptions)
      .pipe(retry(3), catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    let alertMessage!: string;
    let consoleMessage!: string;

    if (error.status === 0) {
      alertMessage = 'Please check your network connection';
      consoleMessage = 'An error occurred: ';
    } else {
      if (error.status === 403) {
        alertMessage = "Invalid credentials"
      } else if (error.status === 409) {
        alertMessage = "User already exists with username or email provided"
      } else {
        alertMessage = "Internal servor error, please try again later"
      }

      consoleMessage = `Backend returned code ${error.status}, body was: `;
    }
    console.error(consoleMessage, error.error);
    alert(alertMessage);
    return throwError(
      'Encountered error during processing; please try again later.'
    );
  }
}
