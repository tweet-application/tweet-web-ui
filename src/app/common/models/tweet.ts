export class Tweet {
  'id': string;
  'username': string;
  'message': string;
  'tag': string;
  'timeAgo': string;
  'likedBy': string[];
  'replies': Tweet[] = [];
}
