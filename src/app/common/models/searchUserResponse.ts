import { Pageable } from './pagination/pageable';
import { Sort } from './pagination/sort';
import { User } from './user';

export class SearchUserResponse {
  'content': User[];
  'pageable': Pageable;
  'last': boolean;
  'totalPages': number;
  'totalElements': number;
  'size': number;
  'number': number;
  'sort': Sort;
  'first': boolean;
  'numberOfElements': number;
  'empty': boolean;
}
