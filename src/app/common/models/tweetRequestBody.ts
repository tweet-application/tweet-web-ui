export class TweetRequestBody {
  'username': string;
  'message': string;
  'tag': string;
}
