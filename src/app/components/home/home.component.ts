import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Tweet } from 'src/app/common/models/tweet';
import { TweetRequestBody } from 'src/app/common/models/tweetRequestBody';
import { SessionService } from 'src/app/common/services/session.service';
import { TweetService } from 'src/app/common/services/tweet.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  private message: FormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(144),
  ]);
  private tag: FormControl = new FormControl('', Validators.maxLength(50));
  private loggedInSubscription!: Subscription;
  private usernameSubscription!: Subscription;
  
  public tweets: Tweet[] = [];
  public username!: string;
  public loggedIn!: boolean;
  public tweetForm = new FormGroup({
    message: this.message,
    tag: this.tag,
  });

  constructor(
    private tweetService: TweetService,
    private session: SessionService
  ) {}

  hasReplies = (_: number, node: Tweet) =>
    !!node.replies && node.replies.length > 0;

  ngOnInit(): void {
    this.loggedInSubscription = this.session.loggedIn$.subscribe(
      (loggedIn) => (this.loggedIn = loggedIn)
    );
    this.usernameSubscription = this.session.userDetails$.subscribe(
      (details) => (this.username = details.username)
    );
    this.tweetService
      .getAllTweets()
      .subscribe((tweets) => (this.tweets = tweets));
  }

  onSubmit() {
    const requestBody: TweetRequestBody = {
      username: this.username,
      message: this.tweetForm.get('message')?.value,
      tag: this.tweetForm.get('tag')?.value,
    };
    this.tweetService
      .postTweet(this.username, requestBody)
      .subscribe((tweet) => this.tweets.unshift(tweet));
  }

  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
    this.usernameSubscription.unsubscribe();
  }
}
