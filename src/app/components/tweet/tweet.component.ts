import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Tweet } from 'src/app/common/models/tweet';
import { TweetRequestBody } from 'src/app/common/models/tweetRequestBody';
import { TweetService } from 'src/app/common/services/tweet.service';

@Component({
  selector: 'tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.scss'],
})
export class TweetComponent {
  @Input() tweet!: Tweet;
  @Input() username!: string;

  private message: FormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(144),
  ]);
  private tag: FormControl = new FormControl('', Validators.maxLength(50));

  public replyForm = new FormGroup({
    message: this.message,
    tag: this.tag,
  });
  public showReplyForm: boolean = false;
  public showReplies: boolean = false;
  public replies: Tweet[] = [];

  constructor(private tweetService: TweetService) {}

  likeTweet(username: string, id: string) {
    this.tweetService.likeTweet(username, id).subscribe();
  }

  toggleShowReplyForm() {
    this.showReplyForm = !this.showReplyForm;
  }

  replyToTweet() {
    const reply: TweetRequestBody = {
      username: this.username,
      message: this.replyForm.get('message')?.value,
      tag: this.replyForm.get('tag')?.value,
    };
    this.tweetService
      .replyToTweet(this.username, this.tweet.id, reply)
      .subscribe((reply) => this.replies.unshift(reply));
  }

  displayReplies(id: string) {
    this.getReplies(id);
    this.showReplies = true;
  }

  hideReplies() {
    this.showReplies = false;
  }

  getReplies(id: string) {
    this.tweetService
      .getTweetReplies(id)
      .subscribe((replies) => (this.replies = replies));
  }
}
