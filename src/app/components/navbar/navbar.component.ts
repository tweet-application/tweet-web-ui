import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/common/services/session.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavBarComponent {
  loggedIn: boolean = false;

  constructor(private router: Router, private session: SessionService) {
    this.session.loggedIn$.subscribe((loggedIn) => (this.loggedIn = loggedIn));
  }

  logOut(): void {
    this.session.logOut();
    alert('Logging out');
    this.router.navigate(['']);
  }
}
